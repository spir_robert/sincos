#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(800, 600);
	paintWidget.kresliOsi(this->ui.spinBox->value());
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::KresliClicked()
{
	paintWidget.clearImage();
	paintWidget.kresliOsi(ui.spinBox->value());
	if (ui.comboBox->currentIndex() == 0)
	{
		if (ui.radioButton->isChecked())paintWidget.kresliSinusB(ui.spinBox->value());
		if (ui.radioButton_2->isChecked())paintWidget.kresliSinusC(ui.spinBox->value());
		if (ui.radioButton_3->isChecked())paintWidget.kresliSinusS(ui.spinBox->value());
	}
	else
	{
		if (ui.radioButton->isChecked())paintWidget.kresliSinusB(ui.spinBox->value(),false);
		if (ui.radioButton_2->isChecked())paintWidget.kresliSinusC(ui.spinBox->value(), false);
		if (ui.radioButton_3->isChecked())paintWidget.kresliSinusS(ui.spinBox->value(), false);
	}

}
